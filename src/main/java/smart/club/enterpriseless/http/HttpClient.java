package smart.club.enterpriseless.http;

import javax.ejb.Local;

/**
 * Created by Alexander on 08.05.2016.
 */
@Local
public interface HttpClient {
    public String get(String url);
}
