package smart.club.enterpriseless.dto;

import smart.club.enterpriseless.model.Currency;

/**
 * Created by Alexander on 08.05.2016.
 */
public class CurrencyDtoConverter {
    public static CurrencyDto toDto(Currency currency) {
        CurrencyDto result = new CurrencyDto();
        result.setCreated(currency.getCreated());
        result.setCurrency(currency.getCurrency());
        result.setRate(currency.getRate());
        return result;
    }
}
